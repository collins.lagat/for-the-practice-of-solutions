export class Request {
  constructor(
    public readonly route: string,
    public readonly method: 'GET' | 'POST',
    public readonly headers: { [k: string]: string },
    public readonly payload?: { [k: string]: unknown }
  ) {}
}

export class Response {
  constructor(
    public readonly code: 200 | 201 | 401 | 422 | 404,
    public readonly headers: { [k: string]: string },
    public readonly body?: string
  ) {}

  public status(code: 200 | 201 | 401 | 422 | 404): Response {
    return new Response(code, this.headers, this.body)
  }

  public message(msg: string) {
    return new Response(this.code, this.headers, msg)
  }
}

// Routing without facade
export const routingWithoutFacade = (request: Request): Response => {
  if (request.method === 'GET' && request.route === '/dummy') {
    return new Response(200, request.headers)
  }

  if (request.method === 'POST' && request.route === '/dummy') {
    if (!request.payload) {
      return new Response(422, request.headers)
    }

    return new Response(201, request.headers, '')
  }

  if (request.method === 'GET' && request.route === '/secret-route') {
    if (
      !request.headers['Authorization'] ||
      request.headers['Authorization'] !== 'Bearer secure-token'
    ) {
      return new Response(401, request.headers)
    }
    return new Response(200, request.headers)
  }

  return new Response(404, request.headers)
}

// ---------------------------------------------------------------------------------

/**
 * Router will act as our facade
 */

class Router {
  private routes: {
    [k: string]: {
      [k in 'GET' | 'POST']?: (request: Request, response: Response) => Response
    }
  } = {}
  private protectedRoutes: string[] = []

  public get(
    route: string,
    callback: (request: Request, response: Response) => Response
  ): void {
    this.route(route, 'GET', callback)
  }

  public post(
    route: string,
    callback: (request: Request, response: Response) => Response
  ): void {
    this.route(route, 'POST', callback)
  }

  private route(
    route: string,
    method: 'GET' | 'POST',
    callback: (request: Request, response: Response) => Response
  ) {
    if (!this.routes[route]) {
      this.routes[route] = {}
    }
    this.routes[route][method] = callback
  }

  public auth(route: string): Router {
    this.protectedRoutes.push(route)
    return this
  }

  private requiresAuth(request: Request): boolean {
    return this.protectedRoutes.includes(request.route)
  }

  private authTokenIsValid(request: Request): boolean {
    return (
      request.headers['Authorization'] !== undefined &&
      request.headers['Authorization'] === 'Bearer secure-token'
    )
  }

  private resolve(
    request: Request
  ): ((request: Request, response: Response) => Response) | null {
    if (!this.routes[request.route]) {
      return null
    }

    const callback = this.routes[request.route][request.method]

    if (!callback) {
      return null
    }

    return callback
  }

  public dispatch(request: Request): Response {
    if (this.requiresAuth(request) && !this.authTokenIsValid(request)) {
      return new Response(401, request.headers)
    }

    const callback = this.resolve(request)

    if (!callback) {
      return new Response(404, request.headers)
    }

    const response = new Response(200, request.headers)

    return callback(request, response)
  }
}

// dispatch request with router facade

export const routingWithFacade = (request: Request): Response => {
  const router = new Router()

  router.get('/dummy', (_, res) => {
    return res
  })

  router.post('/dummy', (req, res) => {
    if (!req.payload) {
      return res.status(422)
    }

    return res.status(201).message('')
  })

  router.auth('/secret-route').get('/secret-route', (_, res) => {
    return res
  })

  return router.dispatch(request)
}
