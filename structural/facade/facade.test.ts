import {
  Request,
  Response,
  routingWithFacade,
  routingWithoutFacade
} from './facade'

describe('Facade Pattern', () => {
  const response200 = new Response(200, {})
  const response201 = new Response(201, {}, '')
  const response422 = new Response(422, {})
  const authResponse401 = new Response(401, {})
  const authResponse200 = new Response(200, {
    Authorization: 'Bearer secure-token'
  })
  const response404 = new Response(404, {})

  describe('Without Facade', () => {
    it('Should return a 200 (success) response', () => {
      const request = new Request('/dummy', 'GET', {})
      expect(routingWithoutFacade(request)).toEqual(response200)
    })

    it('Should return a 201 (created) response', () => {
      const request = new Request('/dummy', 'POST', {}, {})
      expect(routingWithoutFacade(request)).toEqual(response201)
    })

    it('Should return a 422 (Unprocessable Entity) response', () => {
      const request = new Request('/dummy', 'POST', {})
      expect(routingWithoutFacade(request)).toEqual(response422)
    })

    it('Protected route should return a 401 (Unauthorized) response', () => {
      const request = new Request('/secret-route', 'GET', {})
      expect(routingWithoutFacade(request)).toEqual(authResponse401)
    })

    it('Protected route should return a 200 (success) response', () => {
      const request = new Request('/secret-route', 'GET', {
        Authorization: 'Bearer secure-token'
      })
      expect(routingWithoutFacade(request)).toEqual(authResponse200)
    })

    it('Should return 404 (Not Found) for undefined routes', () => {
      const request = new Request('/undefined-route', 'GET', {})
      expect(routingWithoutFacade(request)).toEqual(response404)
    })
  })

  describe('With Facade', () => {
    it('Should return a 200 (success) response', () => {
      const request = new Request('/dummy', 'GET', {})
      expect(routingWithFacade(request)).toEqual(response200)
    })

    it('Should return a 201 (created) response', () => {
      const request = new Request('/dummy', 'POST', {}, {})
      expect(routingWithFacade(request)).toEqual(response201)
    })

    it('Should return a 422 (Unprocessable Entity) response', () => {
      const request = new Request('/dummy', 'POST', {})
      expect(routingWithFacade(request)).toEqual(response422)
    })

    it('Protected route should return a 401 (Unauthorized) response', () => {
      const request = new Request('/secret-route', 'GET', {})
      expect(routingWithFacade(request)).toEqual(authResponse401)
    })

    it('Protected route should return a 200 (success) response', () => {
      const request = new Request('/secret-route', 'GET', {
        Authorization: 'Bearer secure-token'
      })
      expect(routingWithFacade(request)).toEqual(authResponse200)
    })

    it('Should return 404 (Not Found) for undefined routes', () => {
      const request = new Request('/undefined-route', 'GET', {})
      expect(routingWithFacade(request)).toEqual(response404)
    })
  })
})
