interface IRectangle {
  height: number
  width: number
}

interface ITriangle {
  base: number
  height: number
}

export class Rectangle implements IRectangle {
  constructor(public width: number, public height: number) {}
}

export class Triangle implements ITriangle {
  constructor(public base: number, public height: number) {}
}

export class AdaptedTriangle implements IRectangle {
  width: number
  height: number

  constructor(triangle: ITriangle) {
    this.height = triangle.height
    this.width = 0.5 * triangle.base
  }
}

export class Calculator {
  static compute(rect: IRectangle): number {
    const area = rect.width * rect.height
    if (isNaN(area)) {
      throw new Error('Not a number')
    }
    return area
  }
}
