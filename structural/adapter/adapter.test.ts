import { AdaptedTriangle, Calculator, Rectangle, Triangle } from './adapter'

describe('Adapter Pattern', () => {
  it('Calculator used with intended rectangle interface', () => {
    const rect = new Rectangle(50, 60)
    expect(Calculator.compute(rect)).toBe(3000)
  })

  it('Calculator should throw error/ fail when directly used with triangle interface', () => {
    const tri = new Triangle(100, 60)
    expect(() => Calculator.compute(tri as unknown as Rectangle)).toThrowError()
  })

  it('Calculator should not throw Error with Adapted triangle', () => {
    const tri = new Triangle(100, 60)
    const adaptedTri = new AdaptedTriangle(tri)
    expect(() => Calculator.compute(adaptedTri)).not.toThrowError()
  })

  it('Calculator should work with Adapted triangle', () => {
    const tri = new Triangle(100, 60)
    const adaptedTri = new AdaptedTriangle(tri)
    expect(Calculator.compute(adaptedTri)).toBe(3000)
  })
})
