# Definition

Converts the interface of one class to the interface of another. Bridges interface incompatibility.

# Types

Can be achieved through inheritance (a subclass overriding a parent) or by composition (a compatible object wrapping around the incompatible object).
